#ifndef _WORLD
#define _WORLD

// 1D → 2D:
// (Rectangular Conversion Macro)
#define rc_m(x, y) (2*x+y)

#define SECTORLETsize 16384
#define SECTORsize 49152
#define GENbarr 128
#define starCount SECTORsize*3
#define maxPlanets 7

// Allow for the same universe to be gen'd again? :D

// Entity Definitions :D
typedef struct
{
	char *name;
	char *ID; 					// UUID

	int x, y, size;  			 // X, Y, Size of player.
	int movX, movY;  			 // Move X, Y
	int oMovX, oMovY; 			 // Opposite move X, Y
	float movRadius, movAngle;
	float movSpeed, movDemSpeed; // move speed, move demonstation speed

	int orbX, orbY;
	float orbAng;
	float orbRadX, orbRadY;
	int orbSpeed, orbID;
} spaceship;

typedef struct
{
	int x, y, r;
	int movAng, movSpeed;
} entity;

typedef struct 
{
	char *state;
	spaceship ship;
	entity body;
} hero;

// typedef struct
// {
// 	land[] 
// } world;

typedef struct 
{
	int x, y;
	int r, g, b, a;
} star;

typedef struct
{
	int x, y, r;
	int movRad;
	float orbitSpeed, movAng;
	bool orbitDir;
	char *planetClass; // reminder
} planet;

typedef struct 
{
	int x, y, r;
	int planetCount;
	planet planets[maxPlanets]; 
} sun;

typedef struct
{
	int x, y; // h && w = SECTORLETsize
	star stars[starCount];
	bool sun;
	sun Sol;
	bool gen;
} sectorlet;

extern hero player;
extern sectorlet sectorlets[3 * 3]; // [2][2]
extern int sectCurrX, sectCurrY;

#endif
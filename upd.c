#include "worldObj.h"

void updSect(int Sx, int Sy)
{
	int i;
	if (sectorlets[rc_m(Sx, Sy)].gen)
	{
		for (i = 0; i < sectorlets[rc_m(Sx, Sy)].Sol.planetCount; i++)
		{
			if (sectorlets[rc_m(Sx, Sy)].Sol.planets[i].orbitDir == true)
				sectorlets[rc_m(Sx, Sy)].Sol.planets[i].movAng += sectorlets[rc_m(Sx, Sy)].Sol.planets[i].orbitSpeed/2;
			else
				sectorlets[rc_m(Sx, Sy)].Sol.planets[i].movAng -= sectorlets[rc_m(Sx, Sy)].Sol.planets[i].orbitSpeed/2;	

			sectorlets[rc_m(Sx, Sy)].Sol.planets[i].x = cosf(degreesToRadians(sectorlets[rc_m(Sx, Sy)].Sol.planets[i].movAng))*sectorlets[rc_m(Sx, Sy)].Sol.planets[i].movRad + sectorlets[rc_m(Sx, Sy)].Sol.x;
			sectorlets[rc_m(Sx, Sy)].Sol.planets[i].y = sinf(degreesToRadians(sectorlets[rc_m(Sx, Sy)].Sol.planets[i].movAng))*sectorlets[rc_m(Sx, Sy)].Sol.planets[i].movRad + sectorlets[rc_m(Sx, Sy)].Sol.y;
		}
	}
}

void updPlayer()
{
	// Displaying the player.ships direction
	player.ship.movX = cosf(degreesToRadians(player.ship.movAngle))*player.ship.movRadius + player.ship.x;
	player.ship.movY = sinf(degreesToRadians(player.ship.movAngle))*player.ship.movRadius + player.ship.y;
	player.ship.oMovX = cosf(degreesToRadians(player.ship.movAngle))*-player.ship.movRadius + player.ship.x;
	player.ship.oMovY = sinf(degreesToRadians(player.ship.movAngle))*-player.ship.movRadius + player.ship.y;

	// Moving the player.ship with a velocity of movspeed
	player.ship.x = (scrWidth/2)-scrTransX;
	player.ship.y = (scrHeight/2)-scrTransY;
	if (strcmp(player.state, "norm") == 0)
	{
		scrTransX = cosf(degreesToRadians(player.ship.movAngle))*-player.ship.movSpeed + scrTransX;
		scrTransY = sinf(degreesToRadians(player.ship.movAngle))*-player.ship.movSpeed + scrTransY;
	}
	else if (strcmp(player.state, "orbit") == 0)
	{
		player.ship.orbAng += degreesToRadians(player.ship.orbSpeed);

		sun *Sol = &sectorlets[rc_m(sectCurrX, sectCurrY)].Sol;
		scrTransX = (cosf(player.ship.orbAng)*player.ship.orbRadX - Sol->planets[player.ship.orbID].x)+(scrWidth/2);
		scrTransY = (sinf(player.ship.orbAng)*player.ship.orbRadY - Sol->planets[player.ship.orbID].y)+(scrHeight/2); // Partially working...
	}

	// If the angle goes above 360 or below 0...
	if (player.ship.movAngle > 360.0)
	{
		player.ship.movAngle = 0.0;
	}
	else if (player.ship.movAngle < 0)
	{
		player.ship.movAngle = 360.0;
	}
}
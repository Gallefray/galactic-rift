// Libraries
#include "QL.h"
// definitions
#include "worldObj.h"	// Data objects and variables
#include "draw.c"   	// Drawing functions
#include "gen.c" 		// Random generation functions
#include "upd.c" 		// Update functions

hero player;
sectorlet sectorlets[3 * 3]; // [2][2]
int sectCurrX, sectCurrY;
// bool blah = true;

void variables()
{
	
	sectCurrX = 1;
	sectCurrY = 1;
	sectorlets[rc_m(sectCurrX, sectCurrY)].x = player.ship.x-SECTORLETsize/2; 
	sectorlets[rc_m(sectCurrX, sectCurrY)].y = player.ship.y-SECTORLETsize/2;
	// sectorlets[rc_m(1, 0)].x = sectorlets[rc_m(1, 1)].x; 
	// sectorlets[rc_m(1, 0)].y = sectorlets[rc_m(1, 1)].y-SECTORLETsize;

	player.ship.size = 32;

	player.ship.x = (scrWidth/2);
	player.ship.y = (scrHeight/2);

	player.ship.movAngle = 0;
	player.ship.movRadius = 100.0;
	player.ship.movSpeed = 0.0;
	player.ship.movDemSpeed = 6;

	player.state = "norm";
	player.ship.orbSpeed = 0;
}

void loadStuff() // Gets loaded at the beginning of the program
{
	starGen(1, 1);
	int r = randomNum(200, 300);
	int a = randomNum(0, 360);
	systemGen(cosf(degreesToRadians(a))*r + player.ship.x, sinf(degreesToRadians(a))*r + player.ship.y, 1, 1);
	sectorlets[rc_m(1, 1)].gen = true;
}

void logic()
{
	updPlayer();

	updSect(1, 1);
	updSect(1, 2);

}

void collisions()
{
	// Check if the player.ship is inside the sector
	// left side, right side, top side, bottom side
	// printf("Old coordinates == %d\n", player.ship.y);
	if (rectRectCollision(sectorlets[rc_m(sectCurrX, sectCurrY)].x, sectorlets[rc_m(sectCurrX, sectCurrY)].y, SECTORLETsize, SECTORLETsize, player.ship.x, player.ship.y, player.ship.size, player.ship.size))
	{
		if (rectRectCollision(sectorlets[rc_m(sectCurrX, sectCurrY)].x, sectorlets[rc_m(sectCurrX, sectCurrY)].y, 128, SECTORLETsize, player.ship.x, player.ship.y, player.ship.size, player.ship.size))
		{
			// #define bottSect sectorlets[2*1+2]
			int i;
			sectorlets[rc_m(1,2)] = sectorlets[rc_m(1,1)]; // Copy the arrays
			// Adjusting only the Y coordinates because I'm just shifting it down, I'm not altering the X :P
			sectorlets[rc_m(1,2)].y += SECTORLETsize; // Sector 
			for (i = 0; i < starCount; i++)
				sectorlets[rc_m(1,2)].stars[i].y += SECTORLETsize; // Stars

			if (sectorlets[rc_m(1,2)].sun)
			{
				sectorlets[rc_m(1,2)].Sol.y += SECTORLETsize; // Sun

				for (i = 0; i < maxPlanets; i++)
				{
					sectorlets[rc_m(1,2)].Sol.planets[i].y += SECTORLETsize;
				}
			}

			scrTransY = (scrHeight/2)-(sectorlets[rc_m(1,2)].y+128); // Set player to bottom sector
			player.ship.movSpeed = 0; // Stop the ship
		}
	}
}

void controls()
{
	if (strcmp(player.state, "norm") == 0)
	{
		// Change the player.ship's vector
		if (keyInput.left)
			player.ship.movAngle -= player.ship.movDemSpeed;

		if (keyInput.right)
			player.ship.movAngle += player.ship.movDemSpeed;	

		if (keyInput.up)
			player.ship.movSpeed += 2;	

		else if (keyInput.down)
			player.ship.movSpeed -= 2;	

		if (keyInput.s)
			player.ship.movSpeed = 0;
		
		if (keyInput.o)
		{
			int i;
			sun *Sol = &sectorlets[rc_m(sectCurrX, sectCurrY)].Sol;
			for (i = 0; i < Sol->planetCount; i++)
			{
				if (circleCircleCollision(player.ship.x, player.ship.y, player.ship.movRadius, Sol->planets[i].x, Sol->planets[i].y, Sol->planets[i].r))
				{
					player.state = "orbit";
					player.ship.orbID = i;
					player.ship.orbAng = giveAngle(player.ship.x, player.ship.y, Sol->planets[i].x, Sol->planets[i].y);

					player.ship.orbRadX = abs(player.ship.x - Sol->planets[i].x);
					player.ship.orbRadY = abs(player.ship.y - Sol->planets[i].y); 

					player.ship.orbSpeed = 2;
					break;
				}
			}
		}
	}
	else if (strcmp(player.state, "orbit") == 0)
	{
		if (keyInput.up)
		{
			player.ship.orbSpeed += 2;	
		}
		else if (keyInput.down)
		{
			player.ship.orbSpeed -= 2;	
		}
		if (keyInput.o)
		{
			player.state = "norm";
		}
		if (keyInput.b)
		{
			player.state = "land";
			// Unfinished 
		}
	}
	else if (strcmp(player.state, "land") == 0)
	{
		if (keyInput.w)
		{
			player.body.y -= player.body.movSpeed;
		}
		else if (keyInput.s)
		{
			player.body.y -= player.body.movSpeed;
		}
		else if (keyInput.a)
		{
			player.body.x -= player.body.movSpeed;
		}
		else if (keyInput.d)
		{
			player.body.x += player.body.movSpeed;
		}
		if (keyInput.b)
		{
			player.state = "orbit";
		}
	}
}

int main(int argc, char *argv[])
{
	//const int seed = time(NULL);
	srand(1384434602);
	//printf("seed: %d.\n", seed); // 1384395911 is the second seed gen'd to be printed.
	initWindow(1024, 768, false, "Space");

	variables(); // Load the variables 
	loadStuff();
	while (keyInput.ESC != true)
	{
		clear(); // Clear the screen

		// glScale(0.5555555555555, 0.55555555555555555, 1);
		draw();

		update(); // Update keyInput and flip the screen buffers

		logic();
		collisions();
		controls();

		capFrameRate(100); // 80 // 60 FPS gives good performance atm, even with the huge starfield
	}
	printf("\n");
	return 0;
}
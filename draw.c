#include "worldObj.h"

void drawGennedSpace(int sX, int sY)
{
	if (sectorlets[rc_m(sX, sY)].gen == true)
	{
		// printf("Sector %d,%d has been generated \n px:%d, py:%d, sx:%d, sy:%d \n", sX, sY, player.ship.x, player.ship.y, sectorlets[rc_m(sX, sY)].x, sectorlets[rc_m(sX, sY)].y);
		// Draw the stars
		int i; // Captain off all the loops, since 1970! (Captain loopiness)
		for (i = 0; i < starCount; i++)
		{
			//setColour(155, 255, 255, 255); For the dwarf stars :3
			setColour(sectorlets[rc_m(sX, sY)].stars[i].r, sectorlets[rc_m(sX, sY)].stars[i].g, sectorlets[rc_m(sX, sY)].stars[i].b, sectorlets[rc_m(sX, sY)].stars[i].a);
			rect("fill", sectorlets[rc_m(sX, sY)].stars[i].x, sectorlets[rc_m(sX, sY)].stars[i].y, 2, 2); // 4
		}

		// Draw the Sun
		setColour(255, 0, 0, 255);
		circle("fill", sectorlets[rc_m(sX, sY)].Sol.x, sectorlets[rc_m(sX, sY)].Sol.y, sectorlets[rc_m(sX, sY)].Sol.r);
		// if (sY == 2)
		// 	printf("Sol.x = %d, Sol.y = %d.", sectorlets[rc_m(sX, sY)].Sol.x, sectorlets[rc_m(sX, sY)].Sol.y);

		// Draw the planets
		for (i = 0; i < sectorlets[rc_m(sX, sY)].Sol.planetCount; i++)
		{
			setColour(0, 255, 0, 255);
			circle("fill", sectorlets[rc_m(sX, sY)].Sol.planets[i].x, sectorlets[rc_m(sX, sY)].Sol.planets[i].y, sectorlets[rc_m(sX, sY)].Sol.planets[i].r);
		}
	}
}

void draw()
{
	drawGennedSpace(sectCurrX, sectCurrY);
	drawGennedSpace(1, 2);
	// setColour(255, 0, 0, 255);
	// rect("fill", -SECTORLETsize, -SECTORLETsize, 32, 32);
	setColour(0, 0, 255, 255);
	rect("fill", player.ship.x-(player.ship.size/2), player.ship.y-(player.ship.size/2), player.ship.size, player.ship.size);
	setColour(255, 0, 0, 255);
	circle("line", player.ship.x, player.ship.y, player.ship.movRadius);
	setColour(255, 255, 0, 255);
	circle("line", player.ship.movX, player.ship.movY, 10);
	circle("line", player.ship.oMovX, player.ship.oMovY, 2);
	pixel(player.ship.movX, player.ship.movY);

	// if (strcmp(player.state, "orbit") == 0)
	// {
	// 	sun *Sol = &sectorlets[rc_m(sectCurrX, sectCurrY)].Sol;
	// 	line(player.ship.x, player.ship.y, Sol->planets[player.ship.orbID].x, Sol->planets[player.ship.orbID].y);
	// }
	setColour(255, 255, 255, 255);
	// line();
}

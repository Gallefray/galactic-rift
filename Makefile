# build onNT:
# 	gcc main.c -o galacticRift ./libQL.dll -Wall -g -mwindows -lmingw32 -lSDLmain -lSDL

# build forNT:
#  	i486-mingw32-gcc main.c -o galacticRift.exe ./libQL.dll -Wall -g -mwindows -lmingw32 -lSDLmain -lSDL

#Compile for linux, increment the build number, then run it.
build git:
	git commit -a 
	git push

build: main.c QL.h
	gcc -O3 main.c -o galacticRift ./libQL.so -lm -Wall -g 
	sh .incBuildNum
	./galacticRift

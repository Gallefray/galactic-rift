#include "worldObj.h"

void starGen(int sX, int sY)
{
	int i;
	int x = sectorlets[rc_m(sX, sY)].x;
	int y = sectorlets[rc_m(sX, sY)].y;
	for (i = 0; i < starCount; i++)
	{
		sectorlets[rc_m(sX, sY)].stars[i].x = randomNum(x, x+SECTORLETsize);
		sectorlets[rc_m(sX, sY)].stars[i].y = randomNum(y, y+SECTORLETsize);
		sectorlets[rc_m(sX, sY)].stars[i].r = randomNum(0, 255);
		sectorlets[rc_m(sX, sY)].stars[i].g = randomNum(0, 255);
		sectorlets[rc_m(sX, sY)].stars[i].b = randomNum(0, 255);
		sectorlets[rc_m(sX, sY)].stars[i].a = randomNum(0, 255);
	}
}

void systemGen(int StarX, int StarY, int sX, int sY) // sX, sY refer to sectorlet[X][Y], 
{
	sectorlets[rc_m(sX, sY)].sun = true;
	sectorlets[rc_m(sX, sY)].Sol.x = StarX;
	sectorlets[rc_m(sX, sY)].Sol.y = StarY;
	sectorlets[rc_m(sX, sY)].Sol.r = randomNum(50, 100);
	sectorlets[rc_m(sX, sY)].Sol.planetCount = randomNum(1, 7);
	int i;
	for (i = 0; i < sectorlets[rc_m(sX, sY)].Sol.planetCount; i++)
	{
		sectorlets[rc_m(sX, sY)].Sol.planets[i].r = randomNum(40, 100);
		sectorlets[rc_m(sX, sY)].Sol.planets[i].orbitDir = randomNum(0, 1);
		sectorlets[rc_m(sX, sY)].Sol.planets[i].orbitSpeed = randomNumF(0.5, 1); 
		sectorlets[rc_m(sX, sY)].Sol.planets[i].movAng = randomNum(0, 360);
		sectorlets[rc_m(sX, sY)].Sol.planets[i].movRad = randomNum(500, 2000);
		sectorlets[rc_m(sX, sY)].Sol.planets[i].x = cosf(degreesToRadians(sectorlets[rc_m(sX, sY)].Sol.planets[i].movAng))*sectorlets[rc_m(sX, sY)].Sol.planets[i].movRad + sectorlets[rc_m(sX, sY)].Sol.x;
		sectorlets[rc_m(sX, sY)].Sol.planets[i].y = sinf(degreesToRadians(sectorlets[rc_m(sX, sY)].Sol.planets[i].movAng))*sectorlets[rc_m(sX, sY)].Sol.planets[i].movRad + sectorlets[rc_m(sX, sY)].Sol.y;
	}
}
